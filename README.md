# Dash Core - Docker Image

This repo repackages the official Dash Core docker image with scripts for testing with Thorchain.

The `bifrost` Dash client has been built to only accept blocks which have been chainlocked. This requires a long-lived masternode quorum (LLMQ) to be established and healthy, typically of at least 300 masternodes.

This check is disabled for testnet/regtest chains so we can run tests against an unstable test network or a single Dash node.

## Manually Build

Build the dash container:
```
docker build -t registry.gitlab.com/thorchain/devops/dash-core .
```