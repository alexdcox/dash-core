#!/bin/bash

. /scripts/core.sh

printthornodeconfig
echo "BLOCK_TIME               $BLOCK_TIME"

echo "Writing config file to: $configPath"
cat > $configPath <<EOF
regtest=1
discover=0
printtoconsole=1
txindex=1
debug=0
rest=1
server=1
logips=1
printpriority=1
watchquorums=1
allowprivatenet=1
addressindex=1
spentindex=1
rpcuser=$SIGNER_NAME
rpcpassword=$SIGNER_PASSWD
rpcauth=$RPC_AUTH

[regtest]
  rpcallowip=$RPC_ALLOW_IP
  rpcbind=0.0.0.0
EOF

dashd "$@" 1>$logPath &
dashdpid="$!"

waitforverificationprogresscomplete $(hostname)

while true; do
  nextBlockHash="$(dash-cli generate 1 &> /dev/null | jq -r '.[0]')"
  sleep $BLOCK_TIME
done &

echo "Generating $initialBlocks initial blocks..."
dash-cli generate $initialBlocks &> /dev/null

waitforblock $(hostname) $initialBlocks

echo "Following dashd log from start:"

tail -f -n +1 $logPath
