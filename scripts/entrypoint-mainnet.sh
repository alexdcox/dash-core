#!/bin/bash

. /scripts/core.sh

printthornodeconfig

echo "Writing config file to: $configPath"
cat > $configPath <<EOF
discover=0
printtoconsole=1
txindex=1
debug=0
rest=1
server=1
logips=1
printpriority=1
watchquorums=1
allowprivatenet=1
addressindex=1
spentindex=1
rpcuser=$SIGNER_NAME
rpcpassword=$SIGNER_PASSWD
rpcauth=$RPC_AUTH
rpcallowip=$RPC_ALLOW_IP
rpcbind=0.0.0.0
EOF

dashd "$@"